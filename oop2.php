<?php
class StudentInfo{
    public $justOneVariable="Hello BITM";
    public function justOneMethod(){
        echo "Hello World";
    }
    public function __construct()
    {
        echo $this->justOneVariable;
    }
}
$justOneObject = new StudentInfo();
$justOneObject->justOneMethod();